#include <Arduino.h>
#include <ArduinoHardware.h>
#include <ros.h>
#include <geometry_msgs/Twist.h>
#include "main.h"
#include "motor.h"

ros::NodeHandle nh;
ros::Subscriber<geometry_msgs::Twist> sub("cmd_vel", &messageCb );

void setup(){
 erainer_motor_init();
 nh.initNode();
 nh.subscribe(sub);
}

void loop(){
 erainer_motor_process();
 nh.spinOnce();
}
