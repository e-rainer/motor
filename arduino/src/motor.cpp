#include <Arduino.h>
#include <ArduinoHardware.h>
#include <geometry_msgs/Twist.h>
#include "Servo.h"
#include "math.h"
#include "motor.h"
#include "pinout.h"

Servo    motor_left;
Servo    motor_right;
uint32_t lastmsg      = 0;
uint8_t  pwm_left     = NO_SPEED; // neutral
uint8_t  pwm_right    = NO_SPEED; // neutral

void erainer_motor_init(void){
 pinMode(PIN_BREAK, OUTPUT);
 digitalWrite(PIN_BREAK, HIGH);    // for now we always pull up the break

 motor_left.attach(PIN_LEFT_WHEEL);
 motor_left.write(pwm_left);   // neutral
 motor_right.attach(PIN_RIGHT_WHEEL);
 motor_right.write(pwm_right); // neutral
}

void messageCb( const geometry_msgs::Twist& msg){
  double speed_lin = msg.linear.x;
  double speed_ang = msg.angular.z;

  lastmsg = millis();

  double rpm_left  = ((speed_lin * 30.0) / (M_PI * WHEEL_RADUIS)) + ((( WHEEL_SEPARATION / WHEEL_RADUIS) * speed_ang) / 6.0);
  double rpm_right = ((speed_lin * 30.0) / (M_PI * WHEEL_RADUIS)) - ((( WHEEL_SEPARATION / WHEEL_RADUIS) * speed_ang) / 6.0);

  if (rpm_left > MOTOR_FORWARD_RPM){
    pwm_left = MAX_SPEED;
  } else if (rpm_left < MOTOR_REVERSE_RPM){
    pwm_left = MAX_REVERSE;
  } else {
    pwm_left = motor_rpm2pwm[round(rpm_left) + MOTOR_FORWARD_RPM];
  }

  if (rpm_right > MOTOR_FORWARD_RPM){
    pwm_right = MAX_SPEED;
  } else if (rpm_right < MOTOR_REVERSE_RPM){
    pwm_right = MAX_REVERSE;
  } else {
    pwm_right = motor_rpm2pwm[round(rpm_right) + MOTOR_FORWARD_RPM];
  }
}

void erainer_motor_process(void){

  if(lastmsg > millis()){
    lastmsg = millis();
  }

  if((millis() - lastmsg) > MOTOR_TIMEOUT){
    pwm_left  = NO_SPEED;
    pwm_right = NO_SPEED;
  }

  motor_left.write(pwm_left);
  motor_right.write(pwm_right);
}