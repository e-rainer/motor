# motor

[https://atadiat.com/en/e-rosserial-arduino-introduction/]
[https://userpages.uni-koblenz.de/~robbie/images/uploads/2015/09/arduino-robot-platform-EN.pdf]
[http://wiki.ros.org/rosserial_arduino/Tutorials/Arduino%20IDE%20Setup]
[https://www.exp-tech.de/blog/arduino-mega-2560-pinbelegung]


```bash
rosrun rosserial_python serial_node.py /dev/ttyUSB0
rostopic pub toggle_led std_msgs/Empty –once
rosrun teleop_twist_keyboard teleop_twist_keyboard.py
```

## Pinout
| Pin         | Description                    | Functionality |
| :---        |    :----:                      | :---          |
| 00          | RX                             | COM           |
| 01          | TX                             | COM           |
| 02          |                                | INT PWM       |
| 03          |                                | INT PWM       |
| 04          | Right Wheel                    | PWM           |
| 05          |                                | PWM           |
| 06          | Left Wheel                     | PWM           |
| 07          |                                | PWM           |
| 08          |                                | PWM           |
| 09          |                                | PWM           |
| 10          |                                | PWM           |
| 11          |                                | PWM           |
| 12          |                                | PWM           |
| 13          |                                | PWM LED       |
| 14          | TX3                            |               |
| 15          | RX3                            |               |
| 16          | TX2                            |               |
| 17          | RX2                            |               |
| 18          | TX1                            | INT           |
| 19          | RX1                            | INT           |
| 20          | SDA                            | INT           |
| 21          | SCL                            | INT           |
| 22          | Break Wheel                    |               |
| 23          |                                |               |
| 24          |                                |               |
| 25          | Ultrasonic Echo   Front Left   |               |
| 26          | Ultrasonic Listen Front Left   |               |
| 27          | Ultrasonic Echo   Front Center |               |
| 28          | Ultrasonic Listen Front Center |               |
| 29          | Ultrasonic Echo   Front Right  |               |
| 30          | Ultrasonic Listen Front Right  |               |
| 31          | Ultrasonic Echo   Rear Left    |               |
| 32          | Ultrasonic Listen Rear Left    |               |
| 33          | Ultrasonic Echo   Rear Center  |               |
| 34          | Ultrasonic Listen Rear Center  |               |
| 35          | Ultrasonic Echo   Rear Right   |               |
| 36          | Ultrasonic Listen Rear Right   |               |
| 37          |                                |               |
| 38          | Bumper Front Left              |               |
| 39          | Bumper Front Center            |               |
| 40          | Bumper Front Right             |               |
| 41          | Bumper Rear Left               |               |
| 42          | Bumper Rear Center             |               |
| 43          | Bumper Rear Right              |               |
| 44          |                                | PWM           |
| 45          |                                | PWM           |
| 46          |                                | PWM           |
| 47          |                                |               |
| 48          |                                |               |
| 49          |                                |               |
| 50          | MISO                           |               |
| 51          | MOSI                           |               |
| 52          | SCK                            |               |
| 53          | SS                             |               |

## Flash MC
```bash
docker run -it --rm --device=/dev/ttyACM0 test pio run -t upload 
```

rosrun teleop_twist_keyboard teleop_twist_keyboard.py

# Template
``` cpp
#include <ros.h>
ros::NodeHandle nh;

std_msgs::String str_msg;
ros::Publisher pub("any_topic", &str_msg);

void setup(){
...
nh.advertise(pub);
...
}

void loop()
{
pub.publish( &str_msg );
nh.spinOnce();
}
```
docker run -it --rm --network=host ros:melodic-ros-core  bash
rostopic list -v
apt update
apt install ros-melodic-teleop-twist-keyboard

rosrun teleop_twist_keyboard teleop_twist_keyboard.py
