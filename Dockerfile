from ros:melodic

RUN apt update
RUN apt upgrade -y
RUN apt install -y python-pip curl vim mc ros-melodic-rosserial ros-melodic-rosserial-arduino
RUN pip install -U platformio

WORKDIR /motor_ws/src
RUN git clone https://github.com/ros-drivers/rosserial.git

WORKDIR /motor_ws
RUN . /opt/ros/melodic/setup.sh && catkin_make
RUN . /opt/ros/melodic/setup.sh && catkin_make install

COPY arduino /arduino
COPY motor.launch /

WORKDIR /arduino/lib
RUN . /opt/ros/melodic/setup.sh && rosrun rosserial_arduino make_libraries.py .

WORKDIR /arduino
RUN pio run
