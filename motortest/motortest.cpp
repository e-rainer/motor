#include <Servo.h>

#define START_PWM      40
#define END_PWM       110
#define STOP_PWM       90
#define TIMEOUT       (30 * 1000)
#define MOTOR_PIN       4
#define COUNTER_PIN    20
#define BREAK_PIN      22
#define DEBOUNCE       50
#define END_COUNTER    50
#define MOTOR_WARMUP 1000

volatile uint32_t counter    = 0;
uint16_t pwm        = START_PWM;
uint32_t starttime  = 0;

Servo motor;

void buttonChanged(){
  counter++;
}

void setup() {
  Serial.begin(9600);
  Serial.println("Start");
  pinMode(COUNTER_PIN, INPUT);
  pinMode(BREAK_PIN, OUTPUT);
  digitalWrite(BREAK_PIN, HIGH);
  motor.attach(MOTOR_PIN);
  motor.write(STOP_PWM);
  attachInterrupt(digitalPinToInterrupt(COUNTER_PIN), buttonChanged, RISING);
  delay(5000);

  // Initial
  motor.write(pwm);
  delay(MOTOR_WARMUP);
  starttime = millis();
}

void loop() {

  if(counter > (END_COUNTER - 1) || ((millis() - starttime) > TIMEOUT)){
    uint32_t now = millis();
    Serial.print("Counter = ");
    Serial.println(counter);
    Serial.print("PWM = ");
    Serial.println(pwm);
    Serial.print("Time = ");
    Serial.println(now - starttime);
    Serial.print("RPM = ");
    Serial.println( (60000.0 / (now-starttime)) * counter) ;
    Serial.println("===========================================");

    pwm++;
    if(pwm > END_PWM){
      motor.write(STOP_PWM);
      while(1){
       delay(1);
      }
    }
    motor.write(pwm);
    delay(MOTOR_WARMUP);

    counter = 0;
    starttime = millis();
  }
}